#! /bin/bash

source /etc/rustic/rustic.conf
restic -r "sftp:"$username"@"$address":"$path backup $to_backup --exclude-file=/etc/rustic/exclude.conf -p /etc/rustic/password.conf

