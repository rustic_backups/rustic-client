#! /bin/bash

install -m644 rustic-backup.service /etc/systemd/system/
install -m644 rustic-backup.timer /etc/systemd/system/

install backup.sh /usr/bin

install -D rustic.conf /etc/rustic/
install -D exclude.conf /etc/rustic/
touch /etc/rustic/password.conf

install -Dm644 LICENSE /usr/share/licences/restic/
